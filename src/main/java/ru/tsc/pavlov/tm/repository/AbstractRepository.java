package ru.tsc.pavlov.tm.repository;

import ru.tsc.pavlov.tm.api.IRepository;
import ru.tsc.pavlov.tm.exception.entity.EntityNotFoundException;
import ru.tsc.pavlov.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> list = new ArrayList<>();

    @Override
    public void add(final E entity) {
        list.add(entity);
    }

    @Override
    public void remove(final E entity) {
        list.remove(entity);
    }

    @Override
    public List<E> findAll() {
        return list;
    }

    @Override
    public List<E> findAll(Comparator<E> comparator) {
        return list.stream().sorted(comparator).collect(Collectors.toList());
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public boolean existsById(final String id) {
        return findById(id) != null;
    }

    @Override
    public E findById(final String id) {
        return list.stream()
                .filter(e -> e.getId().equals(id))
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public E removeById(String userId, final String id) {
        final E entity = findById(id);
        list.remove(entity);
        return entity;
    }

    @Override
    public Integer getSize() {
        return list.size();
    }

}
