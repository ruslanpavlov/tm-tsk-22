package ru.tsc.pavlov.tm.repository;

import ru.tsc.pavlov.tm.api.repository.ICommandRepository;
import ru.tsc.pavlov.tm.command.AbstractCommand;

import java.util.*;

public class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Override
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @Override
    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    @Override
    public Collection<String> getCommandNames() {
        return new ArrayList<>(commands.keySet());
    }

    @Override
    public Collection<String> getCommandArg() {
        return new ArrayList<>(arguments.keySet());
    }

    @Override
    public AbstractCommand getCommandByName(final String name) {
        return commands.get(name);
    }

    @Override
    public AbstractCommand getCommandByArg(final String arg) {
        return arguments.get(arg);
    }

    @Override
    public void add(final AbstractCommand command) {
        final String arg = command.getArgument();
        final String name = command.getName();
        if (Optional.ofNullable(arg).isPresent()) arguments.put(arg, command);
        if (Optional.ofNullable(name).isPresent()) commands.put(name, command);
    }

}
