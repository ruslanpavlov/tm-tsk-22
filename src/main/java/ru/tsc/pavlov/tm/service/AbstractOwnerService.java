package ru.tsc.pavlov.tm.service;

import ru.tsc.pavlov.tm.api.repository.IOwnerRepository;
import ru.tsc.pavlov.tm.api.service.IOwnerService;
import ru.tsc.pavlov.tm.exception.empty.EmptyIdException;
import ru.tsc.pavlov.tm.exception.entity.EntityNotFoundException;
import ru.tsc.pavlov.tm.model.AbstractOwnerEntity;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerService <E extends AbstractOwnerEntity> extends AbstractService<E> implements IOwnerService<E> {

    private final IOwnerRepository<E> repository;

    public AbstractOwnerService(IOwnerRepository<E> repository) {
        super(repository);
        this.repository = repository;
    }

    @Override
    public void add(final E entity) {
        if (entity == null) throw new EntityNotFoundException();
        repository.add(entity);
    }

    @Override
    public void remove(final E entity) {
        if (entity == null) throw new EntityNotFoundException();
        repository.remove(entity);
    }

    @Override
    public List<E> findAll(final String userId) {
        return repository.findAll(userId);
    }

    @Override
    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        if (comparator == null) return Collections.emptyList();
        return repository.findAll(userId, comparator);
    }

    @Override
    public void clear(final String userId) {
        repository.clear(userId);
    }

    @Override
    public boolean existsById(final String id) {
        if (id == null) throw new EmptyIdException();
        return repository.existsById(id);
    }

    @Override
    public E findById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id);
    }

    @Override
    public E removeById(String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeById(userId, id);
    }

    @Override
    public Integer getSize() {
        return repository.getSize();
    }

}
