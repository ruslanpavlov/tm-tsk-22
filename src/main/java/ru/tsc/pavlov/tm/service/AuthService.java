package ru.tsc.pavlov.tm.service;

import ru.tsc.pavlov.tm.api.repository.IAuthRepository;
import ru.tsc.pavlov.tm.api.service.IAuthService;
import ru.tsc.pavlov.tm.api.service.IUserService;
import ru.tsc.pavlov.tm.enumerated.UserRole;
import ru.tsc.pavlov.tm.exception.empty.EmptyUserLoginException;
import ru.tsc.pavlov.tm.exception.empty.EmptyUserPasswordException;
import ru.tsc.pavlov.tm.exception.entity.UserNotFoundException;
import ru.tsc.pavlov.tm.exception.system.AccessDeniedException;
import ru.tsc.pavlov.tm.model.User;
import ru.tsc.pavlov.tm.util.HashUtil;
import ru.tsc.pavlov.tm.util.StringUtil;

public class AuthService implements IAuthService {

    private final IAuthRepository authRepository;
    private final IUserService userService;

    public AuthService(final IAuthRepository authRepository, final IUserService userService) {
        this.authRepository = authRepository;
        this.userService = userService;
    }

    @Override
    public String getCurrentUserId() {
        final String currentUserId = authRepository.getCurrentUserId();
        return currentUserId;
    }

    @Override
    public void setCurrentUserId(final String currentUserId) {
        authRepository.setCurrentUserId(currentUserId);
    }

    @Override
    public void setUser(final String currentUserId) {
        authRepository.setCurrentUserId(currentUserId);
    }

    @Override
    public boolean isUserAuth() {
        return !StringUtil.isEmpty(authRepository.getCurrentUserId());
    }

    @Override
    public boolean isUserAdmin() {
        final UserRole role = userService.findById(authRepository.getCurrentUserId()).getRole();
        return role.equals(UserRole.ADMIN);
    }

    @Override
    public User login(String login, String password) {
        if (StringUtil.isEmpty(login)) throw new EmptyUserLoginException();
        if (StringUtil.isEmpty(password)) throw new EmptyUserPasswordException(login);
        final User user = userService.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        final String hash = HashUtil.salt(password);
        if (!hash.equals(user.getPassword())) throw new AccessDeniedException();
        setUser(user.getId());
        return user;
    }

    @Override
    public void logout() {
        setUser(null);
    }

    @Override
    public void checkRoles(UserRole... roles) {
        if (roles == null || roles.length == 0) return;
        final User user = userService.findById(getCurrentUserId());
        if (user == null) throw new AccessDeniedException();
        final UserRole role = user.getRole();
        if (role == null) throw new AccessDeniedException();
        for (final UserRole item : roles) {
            if (item.equals(role)) return;
        }
        throw new AccessDeniedException();
    }

}
