package ru.tsc.pavlov.tm.exception.system;

import ru.tsc.pavlov.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException () {
        super("Error. Access denied.");
    }

}
