package ru.tsc.pavlov.tm.exception.empty;

import ru.tsc.pavlov.tm.enumerated.UserRole;
import ru.tsc.pavlov.tm.exception.AbstractException;

public class EmptyUserRoleException extends AbstractException {

   public EmptyUserRoleException() {
        super("Received user role is empty.");
    }

   public EmptyUserRoleException(String value) {
        super("Received user role for user " + value + "is empty.");
    }

    public EmptyUserRoleException(UserRole value) {
        super("Received user role for user " + value + "is empty.");
    }

}
