package ru.tsc.pavlov.tm.command.project;

import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.enumerated.UserRole;
import ru.tsc.pavlov.tm.util.TerminalUtil;

public class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @Override
    public UserRole[] roles() {
        return UserRole.values();
    }

    @Override
    public String getName() {
        return TerminalConst.PROJECT_UPDATE_BY_INDEX;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Update project by index";
    }

    @Override
    public void execute() {
        final String userId = getAuthService().getCurrentUserId();
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        if (!getProjectService().existsByIndex(userId, index)) {
            System.out.println("Incorrect values");
            return;
        }
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        getProjectService().updateByIndex(userId, index, name, description);
        System.out.println("[OK]");
    }

}
