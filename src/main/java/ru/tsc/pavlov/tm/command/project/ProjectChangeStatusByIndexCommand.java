package ru.tsc.pavlov.tm.command.project;

import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.enumerated.Status;
import ru.tsc.pavlov.tm.enumerated.UserRole;
import ru.tsc.pavlov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.pavlov.tm.model.Project;
import ru.tsc.pavlov.tm.util.TerminalUtil;

import java.util.Arrays;

public class ProjectChangeStatusByIndexCommand extends AbstractProjectCommand {

    @Override
    public UserRole[] roles() {
        return UserRole.values();
    }

    @Override
    public String getName() {
        return TerminalConst.TASK_CHANGE_STATUS_BY_INDEX;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Change project status by index";
    }

    @Override
    public void execute() {
        final String userId = getAuthService().getCurrentUserId();
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("[ENTER STATUS]");
        System.out.println((Arrays.toString(Status.values())));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Project project = getProjectService().changeStatusByIndex(userId, index, status);
        if (project == null) throw new ProjectNotFoundException();
    }

}
