package ru.tsc.pavlov.tm.command.project;

import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.enumerated.UserRole;
import ru.tsc.pavlov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.pavlov.tm.model.Project;
import ru.tsc.pavlov.tm.util.TerminalUtil;

public class ProjectStartByIdCommand extends AbstractProjectCommand {

    @Override
    public UserRole[] roles() {
        return UserRole.values();
    }

    @Override
    public String getName() {
        return TerminalConst.PROJECT_START_BY_ID;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Start project by id";
    }

    @Override
    public void execute() {
        final String userId = getAuthService().getCurrentUserId();
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Project project = getProjectService().startById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
    }

}
