package ru.tsc.pavlov.tm.command.user;

import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.enumerated.UserRole;
import ru.tsc.pavlov.tm.model.User;

import java.util.List;

public class UserShowCommand extends AbstractUserCommand {

    @Override
    public UserRole[] roles() {
        return new UserRole[]{UserRole.ADMIN};
    }

    @Override
    public String getName() {
        return TerminalConst.USER_SHOW_COMMAND;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Show list of users";
    }

    @Override
    public void execute() {
        System.out.println("USER LIST:");
        final List<User> users = getUserService().findAll();
        for (final User user : users)
            showUser(user);
    }

}
