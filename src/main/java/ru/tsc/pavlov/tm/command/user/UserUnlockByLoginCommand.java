package ru.tsc.pavlov.tm.command.user;

import ru.tsc.pavlov.tm.enumerated.UserRole;
import ru.tsc.pavlov.tm.exception.system.AccessDeniedException;
import ru.tsc.pavlov.tm.util.TerminalUtil;

public final class UserUnlockByLoginCommand extends AbstractUserCommand {

    @Override
    public UserRole[] roles() {
        return new UserRole[]{UserRole.ADMIN};
    }


    @Override
    public String getName() {
        return "user-unlock-by-login";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Unlock user by login";
    }

    @Override
    public void execute() {
        System.out.println("PLEASE ENTER USER LOGIN:");
        final String login = TerminalUtil.nextLine();
        final String id = serviceLocator.getUserService().findByLogin(login).getId();
        final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        if (id.equals(currentUserId)) throw new AccessDeniedException();
        serviceLocator.getUserService().unlockUserByLogin(login);
        System.out.println("USER SUCCESSFULLY UNLOCKED");
    }

}
