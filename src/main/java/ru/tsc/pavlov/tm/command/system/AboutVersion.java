package ru.tsc.pavlov.tm.command.system;

import ru.tsc.pavlov.tm.command.AbstractCommand;
import ru.tsc.pavlov.tm.constant.TerminalConst;

public class AboutVersion extends AbstractCommand {

    @Override
    public String getName() {
        return TerminalConst.ABOUT;
    }

    @Override
    public String getArgument() {
        return "-v";
    }

    @Override
    public String getDescription() {
        return "Display program info";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.18.0");
    }

}
